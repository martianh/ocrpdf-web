(ns ocrpdf-web.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [selmer.parser :as parser]
            [org.apache.commons.io.FilenameUtils :as jfiles]
            [clojure.java.io :as io]
            [clojure.java.shell :as shell]
            [clojure.string :as str]
            [ring.logger :as logger]
            [ring.adapter.jetty :as jetty]
            [ring.util.response :as response]
            [ring.middleware.stacktrace :refer [wrap-stacktrace]]
            [ring.middleware.multipart-params :as mp]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]])
  (:import (org.apache.commons.io FileUtils)
           (org.apache.commons.io FilenameUtils))
  (:gen-class))

(def tesseract-langs
  '{"afr" "Afrikaans" "amh" "Amharic" "ara" "Arabic" "asm" "Assamese" "aze" "Azerbaijani" "aze_cyrl" "Azerbaijani - Cyrilic" "bel" "Belarusian" "ben" "Bengali" "bod" "Tibetan" "bos" "Bosnian" "bre" "Breton" "bul" "Bulgarian" "cat" "Catalan; Valencian" "ceb" "Cebuano" "ces" "Czech" "chi_sim" "Chinese - Simplified" "chi_tra" "Chinese - Traditional" "chr" "Cherokee" "cos" "Corsican" "cym" "Welsh" "dan" "Danish" "dan_frak" "Danish - Fraktur (contrib)" "deu" "German" "deu_frak" "German - Fraktur (contrib)" "dzo" "Dzongkha" "ell" "Greek, Modern (1453-)" "eng" "English" "enm" "English, Middle (1100-1500)" "epo" "Esperanto" "equ" "Math / equation detection module" "est" "Estonian" "eus" "Basque" "fao" "Faroese" "fas" "Persian" "fil" "Filipino (old - Tagalog)" "fin" "Finnish" "fra" "French" "frk" "German - Fraktur" "frm" "French, Middle (ca.1400-1600)" "fry" "Western Frisian" "gla" "Scottish Gaelic" "gle" "Irish" "glg" "Galician" "grc" "Greek, Ancient (to 1453) (contrib)" "guj" "Gujarati" "hat" "Haitian; Haitian Creole" "heb" "Hebrew" "hin" "Hindi" "hrv" "Croatian" "hun" "Hungarian" "hye" "Armenian" "iku" "Inuktitut" "ind" "Indonesian" "isl" "Icelandic" "ita" "Italian" "ita_old" "Italian - Old" "jav" "Javanese" "jpn" "Japanese" "kan" "Kannada" "kat" "Georgian" "kat_old" "Georgian - Old" "kaz" "Kazakh" "khm" "Central Khmer" "kir" "Kirghiz; Kyrgyz" "kmr" "Kurmanji (Kurdish - Latin Script)" "kor" "Korean" "kor_vert" "Korean (vertical)" "kur" "Kurdish (Arabic Script)" "lao" "Lao" "lat" "Latin" "lav" "Latvian" "lit" "Lithuanian" "ltz" "Luxembourgish" "mal" "Malayalam" "mar" "Marathi" "mkd" "Macedonian" "mlt" "Maltese" "mon" "Mongolian" "mri" "Maori" "msa" "Malay" "mya" "Burmese" "nep" "Nepali" "nld" "Dutch; Flemish" "nor" "Norwegian" "oci" "Occitan (post 1500)" "ori" "Oriya" "osd" "Orientation and script detection module" "pan" "Panjabi; Punjabi" "pol" "Polish" "por" "Portuguese" "pus" "Pushto; Pashto" "que" "Quechua" "ron" "Romanian; Moldavian; Moldovan" "rus" "Russian" "san" "Sanskrit" "sin" "Sinhala; Sinhalese" "slk" "Slovak" "slk_frak" "Slovak - Fraktur (contrib)" "slv" "Slovenian" "snd" "Sindhi" "spa" "Spanish; Castilian" "spa_old" "Spanish; Castilian - Old" "sqi" "Albanian" "srp" "Serbian" "srp_latn" "Serbian - Latin" "sun" "Sundanese" "swa" "Swahili" "swe" "Swedish" "syr" "Syriac" "tam" "Tamil" "tat" "Tatar" "tel" "Telugu" "tgk" "Tajik" "tgl" "Tagalog (new - Filipino)" "tha" "Thai" "tir" "Tigrinya" "ton" "Tonga" "tur" "Turkish" "uig" "Uighur; Uyghur" "ukr" "Ukrainian" "urd" "Urdu" "uzb" "Uzbek" "uzb_cyrl" "Uzbek - Cyrilic" "vie" "Vietnamese" "yid" "Yiddish" "yor" "Yoruba"})

(def upload-dir
  "resources/upload/")

(def ocr-file-name
  (atom ""))

(defn file-exists [dir filename]
  (.exists (io/file (str dir filename))))

(defn copy-file [source-path dest-path]
  (io/copy (io/file source-path) (io/file dest-path)))

(defn tesseract-list-langs []
  (let [cmd (shell/sh "/bin/sh" "-c"
                      "tesseract --list-langs")
        out (:out cmd)]
    (println out)
    (remove #{"osd"}
            (rest (str/split out #"\n")))))

(defn tesseract-map-code-to-name [langs]
  (map #(get tesseract-langs %)
       langs))

(defn tesseract-installed-langs-map []
  (let [codes-list (tesseract-list-langs)
        lang-names (tesseract-map-code-to-name
                    codes-list)]
    (zipmap codes-list lang-names)))

(defn file-upload [params]
  (let [file (get params :file) ; file object map
        file-name (file :filename)
        file-name-escaped (str "'" file-name "'")
        size (FileUtils/byteCountToDisplaySize (file :size))
        actual-file (file :tempfile)
        base-name (FilenameUtils/getBaseName file-name)
        ocr-name (str base-name "-ocr.pdf")
        ocr-name-escaped (str "'" ocr-name "'")
        langs-param (or (get params "langs[]") "eng")
        langs-v (if (vector? langs-param)
                  langs-param
                  (vector langs-param))
        langs (str/join "+" langs-v)
        ocr-langs-full (tesseract-map-code-to-name langs-v)
        tess-langs-map (tesseract-installed-langs-map)]
    ;; no file, so ask user:
    (if (str/blank? file-name)
      (parser/render-file "templates/index.html"
                          {:nofile 'nofile
                           :tess-langs-map tess-langs-map})
      (do
        ;; TODO: test file type / size
        (println "saving file as" file-name "in" upload-dir)
        (copy-file actual-file (str upload-dir file-name))
        (println file-name "saved")
        (println "we will OCR in" langs)
        (let [shell
              ;; actually process the file:
              (shell/sh
               "/bin/sh" "-c"
               (format
                "ocrmypdf -l %s --rotate-pages --clean ./%s%s ./%s%s"
                langs upload-dir file-name-escaped upload-dir ocr-name-escaped))
              exit (:exit shell)
              err (:err shell)
              out (:out shell)
              exists (file-exists upload-dir file-name)]
          (println err)
          (println out)
          (reset! ocr-file-name ocr-name)
          (let [ocr-exists (file-exists upload-dir @ocr-file-name)]
            ;; on result, reload with them:
            (parser/render-file "templates/index.html"
                                {:err err :out out :size size :file
                                 file-name :exists exists :ocr-file
                                 @ocr-file-name :ocr-exists
                                 ocr-exists :tess-langs-map tess-langs-map
                                 :ocr-langs-full ocr-langs-full})))))))

(defn download-file [& _args]
  (println "download-file called.")
  (response/header
   (response/content-type
    (response/resource-response @ocr-file-name {:root "upload"})
    "application/pdf")
   "Content-Disposition"
   "attachment"))

(defn clear-upload-dir []
  (let [dir "resources/upload/"]
    (println (format "deleting upload directory %s..." dir))
    (FileUtils/cleanDirectory (io/file dir))
    (println (format "upload dir %s cleaned." dir))))

(defn render-index []
  (let [tess-langs-map (tesseract-installed-langs-map)]
    (clear-upload-dir)
    (parser/render-file "templates/index.html" {:tess-langs-map tess-langs-map})))

(defroutes app-routes
  (GET "/" []
       (render-index))
  (POST "/" {params :params}
        (file-upload params))
  (GET "/download" []
       (download-file))
  (route/resources "/") ; make css fucking work
  (route/not-found "Not Found"))

(def app
  (-> app-routes
      (wrap-defaults api-defaults)
      (wrap-stacktrace)
      (mp/wrap-multipart-params)
      (logger/wrap-with-logger)))

;; this is only needed because something is preventing cider-connect from
;; working with 'lein ring server'
;; also needed to compile with lein jar
(defn -main [& args]
  (jetty/run-jetty app {:port 3000
                        :join? true
                        :max-idle-time 12000000
                        :thread-idle-timeout 60000000
                        :request-header-size 819200}))

;; use this for local REPL:
;; (.start server) / (.stop server) works from cider repl:
;; i.e. so can use cider-jack-in:
;; (defonce server (jetty/run-jetty #'app
;;                                  {:port 3000
;;                                   ;; don't block repl:
;;                                   :join? false
;;                                   ;; 20 minutes - doesn't work?
;;                                   :max-idle-time 1200000}))

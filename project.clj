(defproject ocrpdf-web "0.1.0-SNAPSHOT"
  :description "OCR a pdf with ocrmypdf and tesseract."
  :url "http://codeberg.org/martianh/ocrpdf-web"
  :license {:name "GPLv3"
            :url "https://www.gnu.org/licenses/gpl-3.0.txt"}
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [compojure "1.6.1"]
                 [selmer "1.12.53"]
                 [clojure-interop/apache-commons-io "1.0.0"]
                 [ring "1.9.5"]
                 [ring/ring-defaults "0.3.2"]
                 [ring-logger "1.1.1"]
                 [nrepl/nrepl "1.0.0"]] ; for cider-connect
  :plugins [[lein-ring "0.12.5"]
            [refactor-nrepl/refactor-nrepl "3.5.5"]
            [cider/cider-nrepl "0.28.5"] ; for cider-connet
            [mx.cider/enrich-classpath "1.9.0"]] ; for cider-connect
  :main ^:skip-aot ocrpdf-web.handler
  :ring {:handler ocrpdf-web.handler/app
         :nrepl {:start? true :port 9000}} ; cider-connect
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.2"]]}
   :uberjar {:aot :all}})
